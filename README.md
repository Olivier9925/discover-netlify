# Netlify introduction

## Instructions

* Fork the project on your own GitHub account

* Deploy the project on Netlify

## Deployment

* Link to the website deployed on Netlify : [Link](https://elastic-bhaskara-43d6f4.netlify.com/)
